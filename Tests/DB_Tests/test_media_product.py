import unittest

from db.Author import Author
from db.db import DataBase
from db.Song import Song
from db.Recorder import Recorder
from db.MediaProduct import MediaProduct


class MediaProductCRUDTest(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(MediaProductCRUDTest, self).__init__(*args, **kwargs)
        self.db = DataBase("test")

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.db.db.dropDatabase()

    def __clear_coll__(self):
        self.db.db["author"].drop()
        self.db.db["recorder"].drop()
        self.db.db["song"].drop()
        self.db.db["mediaproduct"].drop()


    def test_something(self):
        self.__clear_coll__()
        itmoment = Author("in this moment", ["maria brink", "eduard alal"])
        itmoment.commit(self.db)
        smbSong = Song("Sex metal barby", itmoment)
        smbSong.commit(self.db)
        recordUn = Recorder("unknown", "usa")
        recordUn.commit(self.db)
        albom = MediaProduct("single smb itm", recordUn, "2015", [smbSong])
        albom.commit(self.db)
        a = MediaProduct.get_by_id(self.db, albom.id)
        self.assertEqual(albom.name, a.name)
        self.__clear_coll__()




if __name__ == '__main__':
    unittest.main()
