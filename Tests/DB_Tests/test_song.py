import unittest

from db.Author import Author
from db.Song import Song
from db.db import DataBase


class SongTests(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(SongTests, self).__init__(*args, **kwargs)
        self.db = DataBase("test3")

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.db.db.dropDatabase()

    def __clear_coll__(self):
        self.db.db["song"].drop()
        self.db.db["author"].drop()

    def test_creating_song(self):
        self.__clear_coll__()
        a = Author("A", [])
        a.commit(self.db)
        s = Song("S", a)
        s.commit(self.db)
        actual = Song.get_by_author_name(self.db, a.name)
        # did't work because Song don't find by Author name
        self.assertEqual(s.name_song, actual[0].name_song)
        self.__clear_coll__()

    def test_get_by_id(self):
        self.__clear_coll__()
        a1 = Author("A1", [])
        a1.commit(self.db)
        s1 = Song("S1", a1)
        s1.commit(self.db)
        actual = Song.get_by_id(self.db, s1.id)
        self.assertEqual(s1.__dict__(), actual.__dict__())
        self.__clear_coll__()

    def test_change(self):
        self.__clear_coll__()
        a2 = Author("A2", [])
        a2.commit(self.db)
        s2 = Song("S2", a2)
        s2.commit(self.db)
        s2.name_song = "s3"
        s2.commit(self.db)
        actual = Song.get_by_id(self.db, s2.id)
        self.assertEqual(s2.name_song, actual.name_song)
        self.__clear_coll__()

    def test_remove(self):
        self.__clear_coll__()
        a3 = Author("A3", [])
        a3.commit(self.db)
        s3 = Song("S3", a3)
        s3.commit(self.db)
        s3.remove()
        self.__clear_coll__()

if __name__ == '__main__':
    unittest.main()
