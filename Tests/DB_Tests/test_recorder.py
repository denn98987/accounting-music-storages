import unittest

from db.Recorder import Recorder
from db.db import DataBase


class MyTestCase(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(MyTestCase, self).__init__(*args, **kwargs)
        self.db = DataBase("test_recorder")

    def __clear_coll__(self):
        self.db.db["recorder"].drop()

    def test_add(self):
        rec = Recorder("Atlanta", "USA")
        rec.commit(self.db)
        self.assertEqual(rec.id, self.db.get_id_obj(rec))
        self.__clear_coll__()

    def test_prop_get(self):
        rec = Recorder(name="Atlanta", country="USA")
        rec.name = "Otlanta"
        rec.commit(self.db)
        act = Recorder.get_by_id(self.db, rec.id, type(rec))
        self.assertEqual(rec.name, act["name"])
        self.__clear_coll__()

if __name__ == '__main__':
    unittest.main()
