import unittest
from threading import Lock

from db.Author import Author
from db.db import DataBase


class AuthorTests(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(AuthorTests, self).__init__(*args, **kwargs)
        self.db = DataBase("test3")

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.db.db.dropDatabase()

    def __clear_coll__(self):
        self.db.db["author"].drop()

    def test_create_author(self):
        self.__clear_coll__()
        itmoment = Author("in this moment", ["maria brink", "eduard alal"])
        itmoment.commit(self.db)
        actual = Author.get_by_id(self.db, itmoment.id)
        self.assertEqual(itmoment.name, actual.name)

        self.__clear_coll__()

    # def test_add_same_author(self):#
    #     itmoment = Author("in this moment", ["maria brink", "eduard alal"])
    #     itmoment.commit(self.db)
    #     with self.assertRaises(Exception):
    #         itmoment.commit(self.db)
    #     self.__clear_coll__() # this depricated

    def test_get_from_bd(self):
        itmoment = Author("in this moment", ["maria brink", "eduard alal"])
        itmoment.commit(self.db)
        itmoment = Author.get_elements(self.db, {"name": "in this moment"})
        self.assertNotEqual(itmoment, None)
        self.assertEqual(itmoment[0].name, "in this moment")
        self.__clear_coll__()

    def test_add_participant(self):
        itmoment = Author("in this moment", ["maria brink", "eduard alal"])
        itmoment.add_soloist("fag")
        itmoment.commit(self.db)
        actual = Author.get_by_id(self.db, itmoment.id)
        self.assertTrue("fag" in itmoment.soloists)
        self.assertTrue("fag" in actual.soloists)
        self.__clear_coll__()

    def test_get_by_id(self):
        self.__clear_coll__()
        dim = Author("dimm", [])
        dim.commit(self.db)
        fromdb = Author.get_by_id(self.db, dim.id)
        self.assertEqual(dim.name, fromdb.name)
        self.__clear_coll__()

    def test_get_by_name(self):
        self.__clear_coll__()
        dim = Author("dimm", [])
        dim.commit(self.db)
        fromdb = Author.get_by_name(self.db, dim.name)
        self.assertEqual(dim.name, fromdb.name)
        self.__clear_coll__()

    def test_del(self):
        self.__clear_coll__()
        dim = Author("dimm", [])
        dim.commit(self.db)
        id = dim.id
        dim.remove()
        dim.commit(self.db)
        actual = Author.get_by_id(self.db, id)
        self.assertIsNone(actual)
        self.__clear_coll__()

if __name__ == '__main__':
    unittest.main()
