import unittest

from db.Base import Base
from db.db import DataBase


class MyTestCase(unittest.TestCase):
    def __clear_coll__(self, db):
        db["base"].drop()

    def test_something(self):
        b = Base(age=0)
        b["age"] = 18
        act = b["age"]
        self.assertNotEqual(act, 18)

    def test_db(self):
        db = DataBase("test_base")
        b = Base(name="Tom")
        b.commit(db)
        self.__clear_coll__(db.db)

    def test_get_by_name(self):
        db = DataBase("test_base")
        b = Base(name="Tim")
        b.commit(db)
        act = Base.get_by_name(db, "Tim", type(Base()))
        self.assertEqual(b["name"], act["name"])
        self.__clear_coll__(db.db)

    def test_instance(self):
        db = DataBase("test_base")
        b = Base(name="Tim")
        b.commit(db)
        c = Base(name="Tom", wife=b)
        c.commit(db)
        act = c["wife"].id
        self.assertEqual(b.id, act)
        self.__clear_coll__(db.db)

    def test_get_by_id(self):
        db = DataBase("test_base")
        b = Base(name="Tim")
        b.commit(db)
        id = b.id
        act = Base.get_by_id(db, id, type(Base()))
        self.assertEqual(b["name"], act["name"])
        self.__clear_coll__(db.db)

    # def test_get_complicated_obj_of_subclass(self):
    # this test didn't work because this : def __get_params_from_dict(db: DataBase, dict_param: dict):
    #     class Exp(Base):
    #         table_name = "exp"
    #
    #         def __init__(self, **parameters):
    #             super().__init__(**parameters)
    #     db = DataBase("test_base")
    #     b = Exp(name="Tom")
    #     b.commit(db)
    #     c = Exp(name="Tim", wife=b)
    #     c.commit(db)
    #     fromdb = Exp.get_by_id(db, c.id, type(Exp()))
    #     act = fromdb["wife"]["name"]
    #     self.assertEqual(b["name"], act)
    #     self.__clear_coll__(db.db)

    def test_change(self):
        #Warning! It won't work on too repitative data
        db = DataBase("test_base")
        self.__clear_coll__(db.db)
        b = Base(name="Tim")
        b.commit(db)
        b["name"] = "Anton"
        b.commit(db)
        act = Base.get_by_id(db, b.id)
        self.assertEqual(b['name'], act['name'])
        self.__clear_coll__(db.db)


if __name__ == '__main__':
    unittest.main()
