import unittest

from ThinClient import ThinClient
from DBServer import ThickServer


class MyTestCase(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(MyTestCase, self).__init__(*args, **kwargs)
        # thr = threading.Thread(target=self.serv_thick, args=())
        # thr.start()
        self.app = self.serv_thin()

    def serv_thick(self):
        getter = ThickServer("localhost", 8887)
        getter.start_server()
        getter.recieve()
        getter.stop_server()

    def serv_thin(self):
        app = ThinClient("localhost", 8889)
        app.start_server()
        return app

    def test_something(self):
        #it's need to start thick server from terminal
        rec = {'name': 'uni', 'country': 'USA'}
        self.app.request('Recorder', 'add', rec)
        idr = self.app.recieve()
        self.assertIsNotNone(idr['response'])


if __name__ == '__main__':
    unittest.main()
