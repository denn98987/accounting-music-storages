from db.Base import Base
from db.MediaProduct import MediaProduct
from enum import Enum

from db.db import DataBase


class Type(Enum):
    CD = 0
    Cassette = 1
    Vinyl = 2


types = {Type.CD: 'CD', Type.Cassette: 'Cassette', Type.Vinyl: 'Vinyl'}


class Card(Base):
    table_name = 'card'

    def __init__(self, product: MediaProduct, count: int, type: str):
        super().__init__(product=product, count=count, type=type)

    @property
    def product(self):
        return self['product']

    @product.setter
    def product(self, value):
        self['product'] = value

    @property
    def count(self):
        return self['count']

    @count.setter
    def count(self, value):
        if value < 0:
            raise ValueError('Value must be more then 0')
        else:
            self['count'] = value

    @property
    def type_card(self):
        return self['type_card']

    @type_card.setter
    def type_card(self, value):
        self['type_card'] = value

    def addCount(self, adding_count):
        self['count'] += adding_count

    @staticmethod
    def get_by_id(db: DataBase, id: str):
        return super(Card, Card).get_by_id(db, id, Card.get_class_type())

    @staticmethod
    def get_by_product_name(db: DataBase, product_name: str):
        MP = MediaProduct.get_by_name(db, product_name)
        return super(Card, Card).get_elements(db, {'product': MP})

    @staticmethod
    def get_elements(db: DataBase, parameters: {}):
        return super(Card, Card).get_elements(db, parameters, Card.get_class_type())

    @staticmethod
    def get_class_type():
        return type(Card(object, 0, Type.Cassette))