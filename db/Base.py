from bson import DBRef
import copy
from db.db import DataBase


class Base:
    table_name = "base"

    def __init__(self, **parameters):
        self.__new_props = parameters
        self.__props = parameters.copy()
        self.__removed = False
        self.__changed = False
        self.__id = None

    def __dict__(self):
        dicts = self.__replace_all_obj_with_ref(self.__props)
        dicts['id'] = str(self.__id)
        return dicts

    def __setitem__(self, key, value):
        self.__changed = True
        self.__new_props[key] = value

    def __getitem__(self, x):
        if x in self.__props:
            return self.__props[x]
        else:
            raise Exception("Cannot find this field.")

    @property
    def id(self):
        return self.__id

    def set_id(self, db: DataBase):
        """
        Find id in db ans set it
        :param db: DataBase
        """
        self.__id = db.get_id_obj(self)

    def commit(self, db: DataBase):
        """
        Insert, update, remove this obj in DB, it depends on prev actions
        :param db: DataBase obj
        """
        if not self.__changed and not self.__removed:
            self.__id = db.add_n_get_id(self)
        elif self.__changed:
            old = self.__dict__()
            self.__get_updated_props()
            self.__id = db.change(old, self)
            self.__changed = False
        elif self.__removed:
            self.__remove_in_db(db)
        else:
            self.__id = db.get_id_obj(self)

    def remove(self):
        """
        Mark for remove
        """
        self.__removed = True

    def __remove_in_db(self, db: DataBase):
        """
        Remove from DB
        :param db: Obj of DataBase
        """
        db.remove(self)

    def __get_updated_props(self):
        """
        Update properties to new
        """
        self.__props = self.__new_props.copy()

    @staticmethod
    def base_getting(db: DataBase, query: dict, query_type=None):
        """
        Find in DB an object with exact name
        :param db: DataBase object
        :param query: Found parameters
        :param query_type: Type of class for some table
        :return: Array of objects of queried type if it was found else None
        """
        query_type = type(Base()) if query_type is None else query_type
        coll_name = query_type.table_name
        query = Base.__replace_all_obj_with_ref(query)
        queried = db.find(coll_name, query)
        result = []
        for rec in queried:
            id = rec.pop("_id", None)
            idt = rec.pop("id", None)
            params = Base.__get_params_from_dict(db, rec)
            obj = query_type(**params)
            obj.__id = id if idt is None else idt
            result.append(obj)
        return result

    @staticmethod
    def __replace_all_obj_with_ref(dict_params):
        """
        Replace like Base objects with DBRefs
        :param dict_params: dictionary of properties
        :return: list properties with replaced like Base objects
        """
        result = {}
        for key in dict_params.keys():
            if issubclass(type(dict_params[key]), Base):
                result[key] = DBRef(dict_params[key].table_name, dict_params[key].id)
            elif issubclass(type(dict_params[key]), list):
                if issubclass(type(dict_params[key][0]), Base):
                    result[key] = [DBRef(x.table_name, x.id) for x in dict_params[key] if
                               issubclass(type(x), Base)]
                else:
                    result[key] = dict_params[key]
            else:
                result[key] = dict_params[key]
        return result

    @staticmethod
    def get_by_fields(db: DataBase, **kwargs):#in freeze
        """
        Common function for finding like get_by_id
        :param db: DataBase object
        :param kwargs: fields for looking
        """
        pass

    @staticmethod
    def get_by_id(db: DataBase, id: str, query_type=None):
        """
        Find element from DB by id
        :param db: DataBase object
        :param id: Id object
        :param query_type: Type of class for some table
        :return: Obj of subclass of Base
        """
        data = Base.base_getting(db, {"_id": id}, query_type)
        try:
            result = data[0]
        except IndexError:
            return None
        return result

    @staticmethod
    def get_by_name(db: DataBase, name: str, query_type=None):
        """
        Find in DB an object with exact name
        :param db: DataBase object
        :param name: Found name
        :param query_type: Type of class for some table
        :return: Object of queried type if it was found else None
        """
        data = Base.base_getting(db, {"name": name}, query_type)
        try:
            result = data[0]
        except IndexError:
            return None
        return result

    @staticmethod
    def get_elements(db: DataBase, parameters: {}, query_type=None):
        """
        Find all objects with got parameters
        :param query_type: Type of class for some table
        :param parameters: finding obj fields
        :param db: DataBase
        :return: array of objects
        """
        return Base.base_getting(db, parameters, query_type)

    @staticmethod
    def __get_params_from_dict(db: DataBase, dict_param: dict):
        """
        Get DB dict with params and replace DBRef with instance of suitable class
        :param db:DataBase object
        :param dict_param:Record from DB table
        :return: dict params suitable for init
        """
        props = {}
        params = Base.__replace_ref_with_obj(dict_param)
        for key in params.keys():
            key_type = type(params[key])
            if key_type == tuple:
                props[key] = params[key][0].get_by_id(db, params[key][1])
            else:
                props[key] = params[key]
        return props

    @staticmethod
    def __replace_ref_with_obj(props: dict):
        """
        Replace DBRef to pair of class and Id objects
        :param props: Dict from DB
        :return: Dict with replaced DBRef to pair
        """
        result = {}
        for key in props.keys():
            type_for_check = type(props[key])
            if type(DBRef("", "")) == type_for_check:
                name_coll = props[key].collection
                subclasses = Base.__subclasses__()
                subclass = next(x for x in subclasses if x.table_name == name_coll)
                result[key] = (subclass, props[key].id)
            else:
                result[key] = props[key]
        return result
