from db.Base import Base
from db.db import DataBase


class Author(Base):
    table_name = "author"

    def __init__(self, name: str, soloists):
        soloists = list(soloists)
        super().__init__(name=name, soloists=soloists)

    @property
    def name(self):
        return self["name"]

    @name.setter
    def name(self, value):
        self["name"] = value

    @property
    def soloists(self):
        return self["soloists"]

    @soloists.setter
    def soloists(self, value: []):
        self["soloists"] = value

    def add_soloist(self, soloist: str):
        sols = self["soloists"]
        sols.append(soloist)
        self["soloists"] = sols

    def change_soloist(self, old_value: str, new_value: str):
        """
        Update information about soloist
        :param old_value: That soloist will be updated
        :param new_value: That information will be writed instead of old value
        """
        sols = self["soloists"]
        sols[sols.index(old_value)] = new_value
        self["soloists"] = sols

    def remove_soloist(self, soloist: str):
        sols = ["soloists"]
        sols.pop(soloist)
        self["soloists"] = sols

    @staticmethod
    def get_by_id(db: DataBase, id: str):
        return super(Author, Author).get_by_id(db, id, Author.get_class_type())

    @staticmethod
    def get_by_name(db: DataBase, name: str):
        return super(Author, Author).get_by_name(db, name, Author.get_class_type())

    @staticmethod
    def get_elements(db: DataBase, parameters: {}):
        return super(Author, Author).get_elements(db, parameters, Author.get_class_type())

    @staticmethod
    def get_class_type():
        return type(Author("", []))
