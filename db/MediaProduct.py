from db import Recorder
from db.Base import Base
from db.db import DataBase


class MediaProduct(Base):
    table_name = "media_product"

    def __init__(self, name: str, recorder: Recorder, year: str, songs: []):
        super().__init__(name=name, recorder=recorder, year=year, songs=songs)

    @property
    def name(self):
        return self["name"]

    @name.setter
    def name(self, value):
        self["name"] = value

    @property
    def year(self):
        return self["year"]

    @year.setter
    def year(self, value: str):
        self["year"] = value

    @property
    def recorder(self):
        return self["recorder"]

    @recorder.setter
    def recorder(self, value: str):
        self["recorder"] = value

    @property
    def songs(self):
        return self["songs"]

    def add_song(self, song: str):
        songs = self.songs
        songs.append(song)
        self["songs"] = songs

    def change_song(self, old_value: str, new_value: str):
        songs = self.songs
        songs[songs.index(old_value)] = new_value
        self["songs"] = songs

    def remove_song(self, song: str):
        songs = self.songs
        songs.remove(song)
        self["songs"] = songs

    @staticmethod
    def get_by_id(db: DataBase, id: str):
        return super(MediaProduct, MediaProduct).get_by_id(db, id, MediaProduct.get_class_type())

    @staticmethod
    def get_by_name(db: DataBase, name: str):
        return super(MediaProduct, MediaProduct).get_by_name(db, name, MediaProduct.get_class_type())

    @staticmethod
    def get_elements(db: DataBase, parameters: {}):
        return super(MediaProduct, MediaProduct).get_elements(db, parameters, MediaProduct.get_class_type())

    @staticmethod
    def get_class_type():
        return type(MediaProduct("", object, '', []))