from db.Base import Base
from db.db import DataBase


class Recorder(Base):
    table_name = "recorder"

    def __init__(self, name: str, country: str):
        super().__init__(name=name, country=country)

    @property
    def name(self):
        return self["name"]

    @name.setter
    def name(self, value):
        self["name"] = value

    @property
    def country(self):
        return self["country"]

    @country.setter
    def country(self, value):
        self["country"] = value

    @staticmethod
    def get_by_id(db: DataBase, id: str):
        return super(Recorder, Recorder).get_by_id(db, id, Recorder.get_class_type())

    @staticmethod
    def get_elements(db: DataBase, parameters: {}):
        return super(Recorder, Recorder).get_elements(db, parameters, Recorder.get_class_type())

    @classmethod
    def get_class_type(cls):
        return type(Recorder('', ''))
