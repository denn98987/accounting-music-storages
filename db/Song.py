from bson import DBRef
from db.Author import Author
from db.Base import Base
from db.db import DataBase


class Song(Base):
    table_name = "song"

    def __init__(self, name: str, author: Author):
        super().__init__(name=name, author=author)

    @property
    def author(self):
        return self["author"]

    @author.setter
    def author(self, value: Author):
        self["author"] = value

    @property
    def name_song(self):
        return self["name"]

    @name_song.setter
    def name_song(self, value: str):
        self["name"] = value

    @staticmethod
    def get_by_id(db: DataBase, id: str):
        return super(Song, Song).get_by_id(db, id, Song.get_class_type())

    @staticmethod
    def get_by_name(db: DataBase, name: str):
        return super(Song, Song).get_by_name(db, name, Song.get_class_type())

    @staticmethod
    def get_elements(db: DataBase, parameters: {}):
        return super(Song, Song).get_elements(db, parameters, Song.get_class_type())

    @staticmethod
    def get_by_author_name(db: DataBase, author_name: str):
        author = Author.get_by_name(db, author_name)
        return Song.get_elements(db, {"author": author})

    @staticmethod
    def get_class_type():
        return type(Song("", object))
