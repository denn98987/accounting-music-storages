import pymongo


class DataBase:
    def __init__(self, name: str):
        client = pymongo.MongoClient()
        self.db = client[name]

    def save_get_id(self, inserting_obj):
        """
        Save inserting object in DB
        :param inserting_obj: object to inserting
        :return: id of saving record in DB
        """
        coll = self.db[self.get_type_collection(inserting_obj)]
        insert_dict = inserting_obj.__dict__()
        id = coll.insert_one(insert_dict).inserted_id
        return id

    def update(self, updating_obj, new_obj):
        """
        Update old object to new object
        :param updating_obj: old object
        :param new_obj: new object
        """
        upt = updating_obj.__dict__()
        nnew = new_obj.__dict__()
        self.db[self.get_type_collection(updating_obj)].delete_one(upt)
        self.db[self.get_type_collection(new_obj)].insert_one(nnew)
        # self.db[self.get_type_collection(updating_obj)].replace_one(upt, nnew)

    def remove(self, removing_obj):
        """
        Remove one object from DB
        :param removing_obj: object for remove
        """
        self.db[self.get_type_collection(removing_obj)].delete_one(removing_obj.__dict__())

    def find_obj(self, obj):
        """
        Get object from DB by object
        :param obj: object of DB record
        :return: DB object with id
        """
        return self.db[self.get_type_collection(obj)].find_one(obj.__dict__(), {"_id": False})

    def get_obj_by_name(self, collection_name, name: str):
        """
        Gets record from DB from certain collection by name
        :param collection_name: certain collection name
        :param name: name of record
        :return: dictionary with record
        """
        coll = self.db[collection_name]
        return coll.find_one({"name": name})

    def get_id_obj(self, obj):
        """
        Gets id record of obj if it is added in DB
        :param obj: object of DB Record
        :return: Id or None
        """
        result = self.db[self.get_type_collection(obj)].find_one(obj.__dict__(), {"_id": 1})
        return result["_id"] if result is not None else None

    @staticmethod
    def get_type_collection(obj) -> str:
        return type(obj).__name__.lower()

    @staticmethod
    def send_request(db, collection_name, request: dict):
        """
        Send request to DB
        :param db: DataBase like MongoDb object!
        :param collection_name: requesting collection
        :param request: request dictionary
        :return: list of document dictionaries OR None
        """
        return db[collection_name].find(request)

    def find(self, coll_name: str, request: dict):
        """
        Send request to DB
        :param coll_name: Name of collection
        :param request: request dictionary
        :return: list of document dictionaries OR None
        """
        return self.db[coll_name].find(request)

    def change(self, filter, new_obj):
        """
        Update old object to new object / new version. If it doesn't exist then add it.
        :param filter: Dict of old object
        :param new_obj: new object
        :return: id changing record
        """
        replacemant = new_obj.__dict__()
        doc = self.db[new_obj.table_name].replace_one(filter, replacemant)
        upid = doc.upserted_id
        if upid is None:
            upid = self.add_n_get_id(new_obj)
        return upid

    def add_n_get_id(self, inserting_obj):
        """
        Insert object in collection that written in "table name" prop in obj else in bunch of data in DB
        :param inserting_obj: object to inserting
        :return: id of saving record in DB
        """
        coll_name = inserting_obj.table_name if hasattr(inserting_obj, "table_name") else "shit_bunch"
        coll = self.db[coll_name]
        insert_dict = inserting_obj.__dict__()
        if 'id' in insert_dict:
            del insert_dict['id']
        id = coll.insert_one(insert_dict).inserted_id
        return id
