import sys
import pandas as pd
from PyQt5.QtCore import QRect
from PyQt5.QtGui import QPainter

from PyQt5.QtWidgets import *
from PyQt5 import uic

from ThinClient import ThinClient
from ui.TableModel import TableModel
from ui.uihandler import uihandler


class Ui(QMainWindow):
    def __init__(self):
        super(Ui, self).__init__()
        self.init_data = {}
        self.local_data = {}
        self.serv = ThinClient("localhost", 7777)
        self.load_data()
        uic.loadUi('main.ui', self)
        self.types_group = QButtonGroup()
        self.types_group.addButton(self.CDradioButton)
        self.types_group.addButton(self.VinylradioButton)
        self.types_group.addButton(self.CassetteradioButton)
        self.type = "CD"
        self.pages.setCurrentIndex(0)
        self.add_triggers()
        self.add_handlers()
        self.model = TableModel(self.display_data)
        self.MediaTable.setModel(self.model)
        self.SongslistWidget.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.CDradioButton.setChecked(True)
        uihandler(self)
        self.show()

    def add_triggers(self):
        self.actionAdd.triggered.connect(self.load_add_page)
        self.actionFind.triggered.connect(self.load_main_page)

    def load_main_page(self):
        self.pages.setCurrentIndex(0)
        self.table_set()
        self.table_draw()

    def add_handlers(self):
        self.AuthorSaveButton.clicked.connect(self.author_save)
        self.RecorderSaveButton.clicked.connect(self.recorder_save)
        self.SongSaveButton.clicked.connect(self.song_save)
        self.MPSaveButton.clicked.connect(self.mp_save)
        self.CardSaveButton.clicked.connect(self.card_save)
        self.FindButton.clicked.connect(self.finding)
        self.CancelButton.clicked.connect(self.cancel_finding)
        self.types_group.buttonClicked.connect(self.save_type)
        self.MediaTable.doubleClicked.connect(self.cell_click)

    def cell_click(self):
        ind = self.MediaTable.selectedIndexes()[0].row()
        df = self.fdf.merge(self.playdf, how='left', left_on='id_x', right_on='id_x')
        self.w = MyPopup(df)
        self.w.setGeometry(QRect(100, 100, 400, 200))
        self.w.show()


    def cancel_finding(self):
        self.FindEdit.clear()
        self.table_draw()

    def table_draw(self):
        self.model = TableModel(self.display_data)
        self.MediaTable.setModel(self.model)

    def finding(self):
        findstr = self.FindEdit.text()
        rstr = r'^.*'+findstr.lower()+'.*$'
        df = self.display_data
        self.model = TableModel(df[(df['product'].str.contains(rstr))|(df['year'].str.contains(rstr))|
                                   (df['record_studios'].str.contains(rstr))|(df['type'].str.contains(rstr))])
        self.MediaTable.setModel(self.model)

    def save_type(self, button):
        self.type = button.text()

    def card_save(self):
        mp_name = str(self.MPcomboBox.currentText())
        a_iter = next(item for item in self.init_data['MediaProduct'] if item["name"] == mp_name)
        mp_id = a_iter['id']
        count = self.CardspinBox.value()
        params = {'product': mp_id, 'type': self.type, 'count': count}
        id = self.query('Card', 'add', params)
        params['id'] = id
        self.save_local('Card', params)
        self.MPcomboBox.setCurrentIndex(0)
        self.CDradioButton.setChecked(True)
        self.CardspinBox.setValue(0)

    def mp_save(self):
        mp_name = self.MPNameEdit.text()
        mp_year = self.mp_yearlineEdit.text()
        rec_name = str(self.RecordercomboBox.currentText())
        a_iter = next(item for item in self.init_data['Recorder'] if item["name"] == rec_name)
        rec_id = a_iter['id']
        songs_names = [x.text() for x in self.SongslistWidget.selectedItems()]
        songs_ids = [next(item for item in self.init_data['Song'] if item["name"] == sname)['id'] for sname in songs_names]
        params = {'name': mp_name, 'recorder': rec_id, 'year': mp_year, 'songs': songs_ids}
        id = self.query('MediaProduct', 'add', params)
        params['id'] = id
        self.save_local('MediaProduct', params)
        self.MPNameEdit.clear()
        self.mp_yearlineEdit.clear()
        self.RecordercomboBox.setCurrentIndex(0)
        self.SongslistWidget.setCurrentRow(0)
        self.load_widget_data()

    def song_save(self):
        song_name = self.NameSongEdit.text()
        author_name = str(self.AuthorcomboBox.currentText())
        a_iter = next(item for item in self.init_data['Author'] if item["name"] == author_name)
        author_id = a_iter['id']
        params = {'name': song_name, 'author': author_id}
        id = self.query('Song', 'add', params)
        params['id'] = id
        self.save_local('Song', params)
        self.NameSongEdit.clear()
        self.AuthorcomboBox.setCurrentIndex(0)
        self.load_widget_data()

    def author_save(self):
        author_name = self.AuthorNameEdit.text()
        author_members = self.MembersplainTextEdit.toPlainText().split(',')
        params = {'name': author_name, 'soloists': author_members}
        id = self.query('Author', 'add', params)
        params['id'] = id
        self.save_local('Author', params)
        self.AuthorNameEdit.clear()
        self.MembersplainTextEdit.clear()
        self.load_widget_data()

    def recorder_save(self):
        rec_name = self.RecorderNameEdit.text()
        rec_country = self.RecorderCountryEdit.text()
        params = {'name': rec_name, 'country': rec_country}
        id = self.query('Recorder', 'add', params)
        params['id'] = id
        self.save_local('Recorder', params)
        self.RecorderCountryEdit.clear()
        self.RecorderNameEdit.clear()
        self.load_widget_data()

    def save_local(self, table, params):
        if table in self.init_data:
            self.init_data[table].append(params)
        else:
            self.init_data[table] = [{table: params}]

    def query(self, table, action, params):
        return self.serv.request(table, action, params).result()

    def load_data(self):
        recs = self.serv.request('Recorder', 'get', {})
        authors = self.serv.request('Author', 'get', {})
        songs = self.serv.request('Song', 'get', {})
        mp = self.serv.request('MediaProduct', 'get', {})
        cards = self.serv.request('Card', 'get', {})
        self.init_data['Recorder'] = recs.result()['response']
        self.init_data['Author'] = authors.result()['response']
        self.init_data['Song'] = songs.result()['response']
        self.init_data['MediaProduct'] = mp.result()['response']
        self.init_data['Card'] = cards.result()['response']
        self.table_set()

    def table_set(self):
        recdf = pd.DataFrame(self.init_data['Recorder'])
        authdf = pd.DataFrame(self.init_data['Author'])
        songdf = pd.DataFrame(self.init_data['Song'])
        mpdf = pd.DataFrame(self.init_data['MediaProduct'])
        carddf = pd.DataFrame(self.init_data['Card'])
        playldf = songdf.merge(authdf, 'left', left_on='author', right_on='id')
        del playldf['id_y']
        del playldf['soloists']
        del playldf['author']
        playldf = playldf.rename(columns={'name_x': 'name_song', 'name_y': 'author'})
        self.playdf = playldf
        mpwithres = mpdf.merge(recdf, 'left', left_on='recorder', right_on='id')
        del mpwithres['recorder']
        del mpwithres['id_y']
        del mpwithres['country']
        mpwithres = mpwithres.rename(columns={'name_y': 'record_studios', 'name_x': 'product'})
        mpwithres['songs'] = mpwithres['songs'].apply(lambda s: [playldf[playldf['id_x']==x].values.tolist() for x in s])
        self.fdf = mpwithres
        del mpwithres['songs']
        fdf = mpwithres.merge(carddf, 'right', left_on='id_x', right_on='product')
        del fdf['id']
        del fdf['product_y']
        del fdf['id_x']
        fdf = fdf.rename(columns={'product_x': 'product'})
        self.display_data = fdf

    def load_add_page(self):
        self.pages.setCurrentIndex(1)
        self.load_widget_data()

    def load_widget_data(self):
        author_names = [x['name'] for x in self.init_data['Author']] if 'Author' in self.init_data else []
        self.AuthorcomboBox.insertItems(0, author_names)
        recors = [x['name'] for x in self.init_data['Recorder']] if 'Recorder' in self.init_data else []
        self.RecordercomboBox.insertItems(0, recors)
        songs = [x['name'] for x in self.init_data['Song']] if 'Song' in self.init_data else []
        self.SongslistWidget.insertItems(0, songs)
        mps = [x['name'] for x in self.init_data['MediaProduct']] if 'MediaProduct' in self.init_data else []
        self.MPcomboBox.insertItems(0, mps)

class MyPopup(QWidget):
    def __init__(self, display_data):
        QWidget.__init__(self)
        layout = QGridLayout(self)
        self.model = TableModel(display_data)
        MediaTable = QTableView()
        MediaTable.setModel(self.model)
        layout.addWidget(MediaTable)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = Ui()
    app.exec_()