from bson import ObjectId, DBRef

from db.Author import Author
from db.Base import Base
from db.Card import Card
from db.MediaProduct import MediaProduct
from db.Recorder import Recorder
from db.Song import Song
from db.db import DataBase


class MessageHandler:
    tables_linker = {
        'MediaProduct': MediaProduct.get_class_type(),
        'Card': Card.get_class_type(),
        'Song': Song.get_class_type(),
        'Author': Author.get_class_type(),
        'Recorder': Recorder.get_class_type()
    }

    actions = {
        'add': lambda obj, db, kwargs: MessageHandler.add(db, obj, kwargs),
        'remove': lambda obj, db, kwargs: MessageHandler.remove(db, obj, kwargs),
        'change': lambda obj, db, kwargs: MessageHandler.change(db, obj, kwargs),
        'get': lambda obj, db, kwargs: MessageHandler.get(db, obj, kwargs)
    }

    def __init__(self, name_db):
        self.db = DataBase(name_db)

    def handle_request(self, parameters: dict):
        return self.actions[parameters['action']](self.tables_linker[parameters['table_name']], self.db, parameters['parameters'])

    @staticmethod
    def add(db, clss: Base, kwargs):
        ids_names = {
            'author': lambda db, s: Author.get_by_id(db, ObjectId(s)),
            'song': lambda db, s: Song.get_by_id(db, ObjectId(s)),
            'recorder': lambda db, s: Recorder.get_by_id(db, ObjectId(s)),
            'media_product': lambda db, s: MediaProduct.get_by_id(db, ObjectId(s)),
            'songs': lambda db, s: [Song.get_by_id(db, ObjectId(x)) for x in s]
                     }
        for key in ids_names.keys():
            if key in kwargs:
                kwargs[key] = ids_names[key](db, kwargs[key])
        obj = clss(**kwargs)
        obj.commit(db)
        id_o = str(obj.id)
        return id_o

    @staticmethod
    def change(db: DataBase, clss: Base, kwargs: dict):
        old = clss.get_by_id(db, kwargs['from_id'])
        for key_prop in kwargs['to_o'].keys():
            if key_prop in old:
                old[key_prop] = kwargs['to_o'][key_prop]
            else:
                raise Warning(f'{key_prop} didn\'t find and didn\'t add. Should think to add.')
        try:
            old.commit(db)
        except Exception:
            return False
        else:
            return True

    @staticmethod
    def remove(db: DataBase, clss: Base, kwargs: dict):
        rem = clss.get_by_id(db, kwargs['id_o'])
        rem.remove()
        try:
            rem.commit(db)
        except Exception:
            return False
        else:
            return True

    @staticmethod
    def get(db: DataBase, clss: Base, kwargs: dict):
        data = [x.__dict__() for x in clss.get_elements(db, kwargs)]
        for el in data:
            for key in el.keys():
                if issubclass(type(el[key]), DBRef):
                    el[key] = str(el[key].id)
                if issubclass(type(el[key]), list) and issubclass(type(el[key][0]), DBRef):
                    el[key] = [str(x.id) for x in el[key]]
        return data