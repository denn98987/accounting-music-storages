import json
import socket
import struct
import threading
import concurrent.futures


class ThinClient:
    def __init__(self, ip='127.0.0.1', port=7777):
        self.sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
        self.sock.connect((ip, port))

    def handle(self, message):
        try:
            print("Got: {}".format(message))
        except Exception as e:
            print("Error: {}".format(e))
        return json.loads(message.decode('utf-8'))

    def request(self, table_name, action, parameters):
        request_data = json.dumps({'table_name': table_name, 'action': action, 'parameters': parameters }).encode()
        with concurrent.futures.ThreadPoolExecutor() as executor:
            future = executor.submit(self.send, request_data)
        return future

    def send(self, query):
        msg = query
        head = struct.pack('II12s', len(msg), 0, '            '.encode())
        self.sock.sendall(head)
        self.sock.sendall(msg)

        head = self.sock.recv(20)
        size, code, inf = struct.unpack('II12s', head)
        msg = self.sock.recv(size)
        res = self.handle(msg)
        return res


if __name__ == "__main__":
    th = ThinClient()
    thr = th.request('MediaProduct', 'add', {'name': 'mix1', 'recorder': '5ef6e3837cec8f38c6456c37',  "year": "2012", "songs": ["5ef70ebea8e1d3363321dea5", "5ef712a635e7e480f52a4d99"]})
    on = thr.result()
    print(on)
